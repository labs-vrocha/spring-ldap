--**********************************
--Ajuste do NLS_CHARACTERSET
--**********************************

connect sys/oracle as sysdba;
shutdown;
startup restrict;
Alter database character set INTERNAL_USE WE8ISO8859P1;
shutdown immediate;
startup;
connect system/oracle

--**********************************
--Tuning OracleXE
--**********************************

alter system set filesystemio_options=directio scope=spfile;
alter system set disk_asynch_io=false scope=spfile;

--**********************************
--Esquema labuser
--**********************************

create tablespace labuser datafile '/u01/app/oracle/oradata/XE/labuser01.dbf' size 100M online;
create tablespace idx_labuser datafile '/u01/app/oracle/oradata/XE/idx_labuser01.dbf' size 100M;
create user labuser identified by labuser default tablespace labuser temporary tablespace temp;
grant resource to labuser;
grant connect to labuser;
grant create view to labuser;
grant create procedure to labuser;
grant create materialized view to labuser;
alter user labuser default role connect, resource;

--**********************************
--Disabling 8080-http port
--**********************************

Exec dbms_xdb.sethttpport(0);

exit;